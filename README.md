# wechat_music

#### 介绍
调用网易云真实api，开发微信小程序云音乐  
实现功能：  
个人页面登录状态记录  
首页动态获取数据，轮播图、精心推荐、排行榜，跳转每日推荐  
每日推荐动态获取歌单，可以跳转播放页面  
播放页面实现磁盘动画，切歌，暂停/播放  
视频页面动态获取视频，下拉刷新，跳转搜索框  
搜索框记录搜索历史，实现模糊匹配  

#### 软件架构
微信小程序
wxml\wxss\js\ajax

#### 安装教程

1.  在微信开发者工具里面可以调试  
注：如无法使用更新依赖npm update  

#### 使用说明

1. 使用微信开发者工具调试  
2.开启music-sever服务器  
 2.1 npm start   
3.普通测试直接开启模拟器即可  
4.真机测试需要开启内网穿透，建议使用NATAPP，使用方法看官方文档一分钟教程  
5.utils/congfig.js中配置服务器url，request.js中更改接口  
2. 拉取代码调试的时候切记把nodejs-server拿出来

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
