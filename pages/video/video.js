// pages/video/video.js
import request from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
      videoGroupList:[],  //导航标签数据
      navId:[],  //导航栏的数据
      videoList:[],   //视频列表数据
      videoId:[],  //视频标识
      videoUpdateTime:[],  //记录video播放时间
      isTriggerd:true   //标识下拉刷新

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  //获取导航标签数据
  this.getVideoGroupListData();

  //这里存在异步问题
  //获取视频列表数据
  // this.getVideoList(this.data.navId);
  },
  //获取导航数据
  async getVideoGroupListData(){
    let videoGroupListData = await request('/video/group/list');
    this.setData({
      videoGroupList:videoGroupListData.data.slice(0,14),
      navId:videoGroupListData.data[0].id
    }),
     //获取视频列表数据
  this.getVideoList(this.data.navId);
  },
//获取视频列表数据
async getVideoList(navId){
  if(!navId){
    return;
  }
 let videoListData = await request('/video/group' , {id:navId});
//关闭正在加载的消息提示框
wx.hideLoading();

//  console.log(videoListData)
let index =0;
let videoList = videoListData.datas.map(item => {
  item.id = index++;
  return item;
})
 this.setData({
  videoList,
  isTriggerd:false
  })
},

  //点击切换视频列表数据
changeNav(event){
  let navId = event.currentTarget.id;
  //通过id,evnet传参number会自动转换成string
  this.setData({
    navId:navId*1,
    videoList:[]
  })
  //  >>>  <<< 位移运算符
  // 将目标数据线转换成二进制，然后移动指定的位数
  // 移动零位会将非number转换成number值

//显示正在加载
wx.showLoading({
  title:'正在加载'
})

  //传入最新的navId，当前点击的导航栏的navId
  this.getVideoList(this.data.navId);

},

//创建获取video标签的实例
handlepaly(event){
/*找到点击播放的视频，在点击播放下一个视频的时候，关闭上一个视频 */
// 要找到上个视频和本次点击的视频是不是同一个的时候触发
// 单例模式：
// 1 需要创建多个对象的时候，通过一个变量函数，始终保持，只有一个对象
// 2 节省内存
// 工厂函数

let vid = event.currentTarget.id

//关闭上一个播放视频
// vid !== this.vid && this.videoContext && this.videoContext.stop();

// 方法二，只有获取到两个视频的不同vid时,才调用
// if(this.vid !== vid){
//   if(this.videoContext){
//     this.videoContext.stop();
//   }
// }

// this.vid = vid;
//更新data中video的状态
this.setData({
  videoId:vid
})

//创建video标签的实例对象
this.videoContext= wx.createVideoContext(vid);

// 判断视频之前是否有播放过
let {videoUpdateTime} = this.data;
let videoItem = videoUpdateTime.find(item => item.vid === vid);
if(videoItem){
  this.videoContext.seek(videoItem.currentTime);
}

this.videoContext.play();
},


//播放结束的时候触发
handleEnded(event){
  //视频播放结束，从数组中移除播放时长记录
  let {videoUpdateTime} = this.data;
  videoUpdateTime.splice(item =>( item.vid === event.currentTarget.id),1)
  this.setData({
    videoUpdateTime
  })
},

//记录播放时间，更新到data中
handleTimeUpdate(event){
  // console.log(event);
  let videoTimeObj = {vid:event.currentTarget.id , currentTime:event.detail.currentTime};
  let {videoUpdateTime} = this.data;
  //新判断播放时长数组中是否有当前视频的播放记录
  // 如果有，则修改时间为当前时间
  // 如果没有则添加
  let videoItem = videoUpdateTime.find(item => item.vid ===videoTimeObj.vid);
  if(videoItem){
    //存在
    videoItem.currentTime = event.detail.currentTime;
  }else {
   //没有
    videoUpdateTime.push(videoTimeObj);
  }
  //更新状态
  this.setData({
    videoUpdateTime
  })
 },

//下拉刷新
 handeleReFresh(){
  // console.log(123)
  this.getVideoList(this.data.navId);
 },
 //到达 底部上拉加载
 handleTolower(){
  console.log('到达底部，上拉触发更新');
  //数据分页 1前端分页 2后端分页
  // TODO 需要取更新的数据
  
    let newVideoList = [
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_E02A88CF2AE65C449CADD2371C315F18",
          "coverUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/3.jpg",
          "height": 1080,
          "width": 1920,
          "title": "郑州轻工业大学----晚会",
          "description": "李宇春华晨宇合作好炸《西门少年》，真是神仙合作啊！",
          "commentCount": 186,
          "shareCount": 516,
          "resolutions": [
            {
              "resolution": 240,
              "size": 29450044
            },
            {
              "resolution": 480,
              "size": 49904667
            },
            {
              "resolution": 720,
              "size": 72480174
            },
            {
              "resolution": 1080,
              "size": 137540575
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 440000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/1.jpg",
            "accountStatus": 0,
            "gender": 0,
            "city": 445200,
            "birthday": -2209017600000,
            "userId": 3247598879,
            "userType": 0,
            "nickname": "郑州轻工业大学",
            "signature": "",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951164781511120,
            "backgroundImgId": 109951162868128400,
            "backgroundUrl": "http://p1.music.126.net/2zSNIqTcpHL2jIvU6hG0EA==/109951162868128395.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": null,
            "djStatus": 0,
            "vipType": 0,
            "remarkName": null,
            "avatarImgIdStr": "109951164781511113",
            "backgroundImgIdStr": "109951162868128395",
            "avatarImgId_str": "109951164781511113"
          },
          "urlInfo": {
            "id": "E02A88CF2AE65C449CADD2371C315F18",
            "url": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/video/1.mp4",
            "size": 137540575,
            "validityTime": 1200,
            "needPay": false,
            "payInfo": null,
            "r": 1080
          },
          "videoGroup": [
            {
              "id": -8013,
              "name": "#人气飙升榜#",
              "alg": "groupTagRank"
            },
            {
              "id": 23118,
              "name": "华晨宇",
              "alg": "groupTagRank"
            },
            {
              "id": 59101,
              "name": "华语现场",
              "alg": "groupTagRank"
            },
            {
              "id": 59108,
              "name": "巡演现场",
              "alg": "groupTagRank"
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": "groupTagRank"
            },
            {
              "id": 58100,
              "name": "现场",
              "alg": "groupTagRank"
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": "groupTagRank"
            }
          ],
          "previewUrl": null,
          "previewDurationms": 0,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "西门少年 (Live)",
              "id": 1442842608,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 861777,
                  "name": "华晨宇",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 8327,
                  "name": "李宇春",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [
                "原唱：李宇春"
              ],
              "pop": 100,
              "st": 0,
              "rt": "",
              "fee": 8,
              "v": 4,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 88421277,
                "name": "歌手·当打之年 第12期",
                "picUrl": "http://p3.music.126.net/mOa6Y35QQa2-A5HArd58sQ==/109951164933975773.jpg",
                "tns": [],
                "pic_str": "109951164933975773",
                "pic": 109951164933975780
              },
              "dt": 293850,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 11756205,
                "vd": -26779
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 7053741,
                "vd": -24264
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 4702509,
                "vd": -22819
              },
              "a": null,
              "cd": "01",
              "no": 7,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 0,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 1416682,
              "mv": 0,
              "publishTime": 0,
              "privilege": {
                "id": 1442842608,
                "fee": 8,
                "payed": 0,
                "st": 0,
                "pl": 128000,
                "dl": 0,
                "sp": 7,
                "cp": 1,
                "subp": 1,
                "cs": false,
                "maxbr": 999000,
                "fl": 128000,
                "toast": false,
                "flag": 68,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "E02A88CF2AE65C449CADD2371C315F18",
          "durationms": 302891,
          "playTime": 363031,
          "praisedCount": 5362,
          "praised": false,
          "subscribed": false
        }
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_510C8F5A9DA8DC23997E522B85F1FB3A",
          "coverUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/2.jpg",
          "height": 720,
          "width": 1280,
          "title": "郑州轻工业大学----我们毕业了",
          "description": "Wiz Khalifa和Charlie Puth在中场休息时为科比演唱的《See you again》\n\n“It's been a long day without you my friend”\n\n#Charlie Puth#|#Wiz Khalifa#",
          "commentCount": 139,
          "shareCount": 317,
          "resolutions": [
            {
              "resolution": 240,
              "size": 17757824
            },
            {
              "resolution": 480,
              "size": 28683079
            },
            {
              "resolution": 720,
              "size": 36021554
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 1000000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/1.jpg",
            "accountStatus": 0,
            "gender": 0,
            "city": 1006600,
            "birthday": 818804058083,
            "userId": 391194506,
            "userType": 200,
            "nickname": "郑州轻工业大学",
            "signature": "",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951165359708720,
            "backgroundImgId": 109951165311536080,
            "backgroundUrl": "http://p1.music.126.net/TsZo0EMK_FECf9sHJkx8RQ==/109951165311536074.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": [
              "欧美"
            ],
            "experts": {
              "1": "音乐视频达人",
              "2": "音乐图文达人"
            },
            "djStatus": 10,
            "vipType": 0,
            "remarkName": null,
            "avatarImgIdStr": "109951165359708723",
            "backgroundImgIdStr": "109951165311536074",
            "avatarImgId_str": "109951165359708723"
          },
          "urlInfo": {
            "id": "510C8F5A9DA8DC23997E522B85F1FB3A",
            "url": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/video/2.mp4",
            "size": 36021554,
            "validityTime": 1200,
            "needPay": false,
            "payInfo": null,
            "r": 720
          },
          "videoGroup": [
            {
              "id": -8003,
              "name": "#点赞榜#",
              "alg": "groupTagRank"
            },
            {
              "id": 16194,
              "name": "Charlie Puth",
              "alg": "groupTagRank"
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": "groupTagRank"
            },
            {
              "id": 58100,
              "name": "现场",
              "alg": "groupTagRank"
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": "groupTagRank"
            }
          ],
          "previewUrl": null,
          "previewDurationms": 0,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "See You Again",
              "id": 401722227,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 90331,
                  "name": "Charlie Puth",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 46006,
                  "name": "Wiz Khalifa",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [
                "电影《速度与激情7》片尾曲"
              ],
              "pop": 100,
              "st": 0,
              "rt": null,
              "fee": 1,
              "v": 16,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 3270972,
                "name": "Nine Track Mind",
                "picUrl": "http://p3.music.126.net/OVHar05vedbWFEWHuArbGA==/3295236348738229.jpg",
                "tns": [],
                "pic": 3295236348738229
              },
              "dt": 229564,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 9184696,
                "vd": -11799
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 5510834,
                "vd": -9300
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3673904,
                "vd": -7799
              },
              "a": null,
              "cd": "1",
              "no": 13,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 1,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 7002,
              "mv": 393006,
              "publishTime": 1454254457074,
              "privilege": {
                "id": 401722227,
                "fee": 1,
                "payed": 0,
                "st": 0,
                "pl": 0,
                "dl": 0,
                "sp": 0,
                "cp": 0,
                "subp": 0,
                "cs": false,
                "maxbr": 320000,
                "fl": 0,
                "toast": false,
                "flag": 1284,
                "preSell": false
              }
            },
            {
              "name": "See You Again",
              "id": 30953009,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 46006,
                  "name": "Wiz Khalifa",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 90331,
                  "name": "Charlie Puth",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [
                "电影《速度与激情7》致敬保罗沃克插曲"
              ],
              "pop": 100,
              "st": 0,
              "rt": null,
              "fee": 1,
              "v": 289,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 3104138,
                "name": "Furious 7 (Original Motion Picture Soundtrack)",
                "picUrl": "http://p3.music.126.net/JIc9X91OSH-7fUZqVfQXAQ==/7731765766799133.jpg",
                "tns": [],
                "pic": 7731765766799133
              },
              "dt": 230520,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 9223358,
                "vd": -17300
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 5534032,
                "vd": -14800
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3689369,
                "vd": -13400
              },
              "a": null,
              "cd": "1",
              "no": 7,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 2,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 7002,
              "mv": 393006,
              "publishTime": 1426521600007,
              "tns": [
                "有缘再见"
              ],
              "privilege": {
                "id": 30953009,
                "fee": 1,
                "payed": 0,
                "st": 0,
                "pl": 0,
                "dl": 0,
                "sp": 0,
                "cp": 0,
                "subp": 0,
                "cs": false,
                "maxbr": 999000,
                "fl": 0,
                "toast": false,
                "flag": 1284,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "510C8F5A9DA8DC23997E522B85F1FB3A",
          "durationms": 228739,
          "playTime": 418612,
          "praisedCount": 3532,
          "praised": false,
          "subscribed": false
        }
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_A54AEB1C28CCD7990E7E6EAB56ECA485",
          "coverUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/1.jpg",
          "height": 486,
          "width": 864,
          "title": "郑州轻工业大学航拍",
          "description": null,
          "commentCount": 21,
          "shareCount": 18,
          "resolutions": [
            {
              "resolution": 240,
              "size": 13805898
            },
            {
              "resolution": 480,
              "size": 13565707
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 440000,
            "authStatus": 1,
            "followed": false,
            "avatarUrl": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/cover/%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9/1.jpg",
            "accountStatus": 0,
            "gender": 1,
            "city": 440100,
            "birthday": 953136000000,
            "userId": 247337423,
            "userType": 4,
            "nickname": "郑州轻工业大学",
            "signature": "来不及.",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 7795537441726101,
            "backgroundImgId": 18641120139551856,
            "backgroundUrl": "http://p1.music.126.net/4ZlZ1W4gaf6UwR1wFVA0XQ==/18641120139551855.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "二次元视频达人"
            },
            "djStatus": 10,
            "vipType": 11,
            "remarkName": null,
            "avatarImgIdStr": "7795537441726101",
            "backgroundImgIdStr": "18641120139551855"
          },
          "urlInfo": {
            "id": "A54AEB1C28CCD7990E7E6EAB56ECA485",
            "url": "https://gulimall-i.oss-cn-beijing.aliyuncs.com/wx/video/3.mp4",
            "size": 13565707,
            "validityTime": 1200,
            "needPay": false,
            "payInfo": null,
            "r": 480
          },
          "videoGroup": [
            {
              "id": 60101,
              "name": "日语现场",
              "alg": "groupTagRank"
            },
            {
              "id": 59108,
              "name": "巡演现场",
              "alg": "groupTagRank"
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": "groupTagRank"
            },
            {
              "id": 3102,
              "name": "二次元",
              "alg": "groupTagRank"
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": "groupTagRank"
            },
            {
              "id": 58100,
              "name": "现场",
              "alg": "groupTagRank"
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": "groupTagRank"
            }
          ],
          "previewUrl": null,
          "previewDurationms": 0,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "A54AEB1C28CCD7990E7E6EAB56ECA485",
          "durationms": 360000,
          "playTime": 29579,
          "praisedCount": 392,
          "praised": false,
          "subscribed": false
        }
      }
    ]
    let videoList = this.data.videoList;
    //将视频最新的数据更新到原有的视频列表中
    videoList.push(...newVideoList)
    this.setData({
      videoList
    })

 },

//  点击搜索跳转到搜索页面
tosearchMusic(){
  wx.navigateTo({
    url: '/pages/search/search',
  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function ({from}) {
    if(fro === 'button'){
        return {
              title:'来自button的转发内容',
              page:'/pages/video/video',
              imageUrl:'/static/images/personal/bgImg.jpg'
            }
    }else{
      return {
        title:'来自menu的转发内容',
        page:'/pages/video/video',
        imageUrl:'/static/images/personal/bgImg.jpg'
      }
    }
    
  }
})