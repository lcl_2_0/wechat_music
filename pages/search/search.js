// pages/search/search.js
import request from '../../utils/request'
let isSend=false; //函数节流使用
Page({

  /**
   * 页面的初始数据
   */
  data: {
    placeholderContent:'', //搜索框默认内容
    hotList:[],
    searchContent:'', //用户输入的表单项数据
    searchListData:[], //关键字模糊匹配的数据
    historyList:[]  //搜索历史记录

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:async function (options) {
    // 获取搜索框默认搜索内容值
    let placeholderContentData =await request('/search/default');
    // 获取热搜榜的数据
    let hotListData = await request('/search/hot/detail');
    this.setData({
      placeholderContent:placeholderContentData.data.showKeyword,
      hotList:hotListData.data
    })
this.getSearchHistory();
    
  },
// input表单项发生改变回调的内容
 inputChange(event){
      // console.log(event)
    this.setData({
      searchContent:event.detail.value.trim()
    })
    if(isSend){
      return
    }
    isSend=true;
    this.getSend();
    // 此处要函数节流
    setTimeout( ()=>{
        isSend = false
    },300)
      
},
// 获取本地搜索历史记录
getSearchHistory(){
  let historyList = wx.getStorageSync('searchHistory');
  if(historyList){
    this.setData({
      historyList
    })
  }
},
// 封装请求模糊搜索的函数
async getSend(){
  if(!this.data.searchContent){
    this.setData({
      searchListData:[]
    })
    return
  }
  let {searchContent , historyList} = this.data
          //  发生请求搜索模糊匹配
      let searchListData = await request('/search' ,{keywords:this.data.searchContent , limit:10});
      this.setData({
        searchListData:searchListData.result.songs
      })

      // 搜索关键字添加到历史记录中
      if(historyList.indexOf(searchContent) !== -1){
        historyList.splice(historyList.indexOf(searchContent),1)
      }
      historyList.unshift(searchContent);
      this.setData({
        historyList
      })

},

// 清空搜索内容
clearSearchContent(){
  // console.log(45)
  this.setData({
    searchContent:'',
    searchListData:''
  })
},

// 删除本地搜索记录
Delete(){
  wx.showModal({
    cancelColor: '确认删除吗？',
    success:(res)=>{
       if(res.confirm){
          // 清空data
            this.setData({
              historyList:[]
            })
            // 清空本地缓存
            wx.removeStorageSync('searchHistory')
       }
    }
  })
  
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})