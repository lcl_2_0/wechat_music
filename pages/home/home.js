// pages/home/home.js
import request from '../../utils/request.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerList:[],  //轮播图数据
    recommendList:[],  //推荐歌单数据
    topList:[]  //排行榜数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:async  function (options) {
    let result = await request('/banner',{type:2});
    // console.log('cg:',result)
    this.setData({
      bannerList:result.banners
    })


    //获取推荐歌单数据
    let recommendListData =await request('/personalized' , {limit:10})
    this.setData({
      recommendList:recommendListData.result
    })

    //后去排行榜数据
    /*
    根据idx的值获取对应的数据
    idx的取值范围是0-20，现在需要0-4
    */
   let index = 0;
   let resultArr = [];
   while (index < 5){
     let topListData =await request('/top/list' , {idx:index++});
     //splice（会修改原数组） slice(不会修改原数组)
     let topListItem = {name:topListData.playlist.name , tracks:topListData.playlist.tracks.slice(0,3)};
     resultArr.push(topListItem);

     this.setData({
      topList:resultArr
    }) //不需要五次请求完成才更新，渲染次数多一些
    }
     //更新topList数值
     //放在此处更新会导致用户体验差，长时间白屏
    // this.setData({
    //   topList:resultArr
    // })
  },
  // 跳转到每日推荐
  toRecomMusic(){
  wx.navigateTo({
    url: '/pages/recomMusic/recomMusic',
  })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})