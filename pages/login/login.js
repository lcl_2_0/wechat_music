// pages/login/login.js
//登录流程
/*1、收集表单数据
2、前端验证
    验证用户信息（账号，密码是否合法）
    验证不通过提示用户，不发请求
    发送，携带账号密码给服务器
3、后端验证
   验证用户是否存在
   用户不存在直接返回，告诉前端不存在
   用户存在需要验证密码是否准确
   密码不正确返回前端密码不正确
   密码准确返回前端数据，提示用户登录成功（携带用户的相关信息）
 */
import request from '../../utils/request.js'
 //当前，手机号，密码两个表单项
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:'', //手机号
    password:''  //用户密码
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 表单收集phone password的秘密
handleInput(event){
  // console.log(event.detail.value)
  // let type = event.currentTarget.id;  //获取phone password  id传值的方法

  let type = event.currentTarget.dataset.type
  //data-  方式传值

  //区别id传一个唯一标识   data- 可以多个  
  // console.log(type ,event.detail.value);
  this.setData({
    [type]:event.detail.value
  })
},

//登录的回调函数
async login(){
  let {phone , password} = this.data;

  if(!phone){
    //提示
    wx.showToast({
      title: '手机号不能为空',
      icon:'none'
    })
    return;
  };
    //正则表达式验证手机号
    let phoneReg = /^1(3|4|5|6|7|8|9)\d{9}$/;
    if(!phoneReg.test(phone)){
      wx.showToast({
        title:'手机号格式不正确',
        icon:'none'
      })
    return;
  }
  if(!password){
    wx.showToast({
      title: '密码不能为空',
      icon:'none'
    })
  }

  //后端验证
  let result = await request('/login/cellphone' , {phone , password ,isLogin:true})

  //返回的状态码验证登录状态
  if(result.code === 200){
    //登录成功
    wx.showToast({
      title: '登录成功'
    })

    //将用户信息存储至本地
    wx.setStorageSync('userInfo' , JSON.stringify(result.profile))

    //登录成功后，跳转个人中心页面
    wx.reLaunch({
      url: '/pages/personal/personal',
    })

  }else if(result.code === 502){
    wx.showToast({
      title: '密码错误',
      icon:'none'
    })
  }else if(result.code === 400){
    wx.showToast({
      title: '手机号错误错误',
      icon:'none'
    })
  }else {
    wx.showToast({
      title: '登录失败，请重新登录',
      icon:'none'
    })
  }
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})