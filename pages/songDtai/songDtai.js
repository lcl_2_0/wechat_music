// pages/songDtai/songDtai.js
import PubSub from 'pubsub-js'
import moment from 'moment'
import request from '../../utils/request'
// 获取全局实例
const appInstance = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isPlay:false , //音乐是否播放的标记
    song:[],  //歌曲详情
    musicId:'',  //音乐id
    currentTime:'00:00', //播放时间
    durationTime:'00:00', //时长
    currentwidth:0   //控制进度条的长度行走，内联
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options) ;
    // options接收路由跳转的query参数
    // 原生小程序路由传参，对参数长度有限制，如果过长会自动截取
    // console.log(typeof options.musicId);
    // console.log(options.musicId);
    // console.log(JSON.parse(options.song))
    let musicId = options.musicId;
    this.getMusicInfo(musicId);
    this.setData({
      musicId
    })

    // 判断当前页面音乐是否在播放
if(appInstance.globalData.isNusicPlay && appInstance.globalData.musicId === musicId){
  // 修改当前页面音乐播放状态为true
  this.setData({
    isPlay:true
  })
  
}

 // 创建音频播放的实例
    this.backgroundAudioManager = wx.getBackgroundAudioManager();

// 接收系统控制的暂停播放的按钮，解决系统暂停导致页面错误显示的问题
// 通过backgroundAudioManager  去监视
this.backgroundAudioManager.onPlay(() =>{
      // console.log("页面")
      this.changeisPlay(true);

      // 修改全局变量
      appInstance.globalData.musicId = musicId;
      });
      this.backgroundAudioManager.onPause(() =>{
        // console.log("系统暂停")
        this.changeisPlay(false);
      });
      this.backgroundAudioManager.onStop(() =>{
        // console.log("wx页面暂停")
        this.changeisPlay(false);
      });
      this.backgroundAudioManager.onEnded(()=>{
        // 监听音乐播放完,并且自动下一首，并把进度条还原到0；直接发布切换下一首的消息即可next
          // 发布消息给页面recomMusic
          PubSub.publish('switchType' , 'next')
          this.setData({
            currentwidth:0,
            currentTime:'00:00'
          })
      })

      // 监听音乐实时播放的进度，调用微信api监听
      this.backgroundAudioManager.onTimeUpdate(()=>{
        // console.log('总时长',this.backgroundAudioManager.duration);
        // console.log('实时时间',this.backgroundAudioManager.currentTime);
        // 格式化时间
        let currentTime = moment(this.backgroundAudioManager.currentTime * 1000).format('mm:ss');
        let currentwidth =(this.backgroundAudioManager.currentTime/ this.backgroundAudioManager.duration)*450
        this.setData({
          currentTime,
          currentwidth
        })
      })
  },
changeisPlay(isPlay){

  this.setData({
    isPlay
  })
   // 修改全局变量 音乐播放状态
appInstance.globalData.isNusicPlay = isPlay;
},
  // 获取歌曲详情
 async getMusicInfo(musicId){
    let songData = await request('/song/detail',{ids:musicId});

    // 获取时间 单位ms 使用moment格式化
    let durationTime = moment(songData.songs[0].dt).format('mm:ss');
    this.setData({
      song:songData.songs[0],
      durationTime
    })
    //动态修改窗口标题
    wx.setNavigationBarTitle({
      title: this.data.song.name,
    })
  },

  // 点击播放按钮，播放暂停
  showIsPlay(){
    let isPlay = !this.data.isPlay;
    // this.setData({
    //   isPlay
    // })
    //this.backgroundAudioManager.onPlay(() =>{
// console.log("页面")}) ,可以监听到页面暂停，，onPause可以监听到系统暂停
let {musicId} = this.data;
    this.musicControl(isPlay , musicId);
  },

  // 小程序API背景音频
  //控制音乐播放、暂停的功能函数
  async musicControl(isPlay , musicId){
 
    if(isPlay){
        // 获取音乐连接
        let musicLinkData = await request('/song/url',{id:musicId})
        let musicLink = musicLinkData.data[0].url;

        //播放音乐
      
        // backgroundAudioManager.src = '音乐连接';
        this.backgroundAudioManager.src = musicLink;
        this.backgroundAudioManager.title = this.data.song.name;

    }else{
      //暂停音乐
      this.backgroundAudioManager.pause();
    }

  },

  // 点击上一首，下一首音乐
 switchMusic(event){
   let type = event.currentTarget.id

  //  点击切歌的时候先关闭当前歌曲
  
  //  console.log(type)
  //  console.log(event)

  // 订阅来自recomMusic的musicId消息
  PubSub.subscribe('musicId' , (msg , musicId) =>{
    console.log(musicId);

    // 获取音乐详情
    this.getMusicInfo(musicId);
    // 点击下一首或者上一首的时候自动播放音乐
    this.musicControl(true , musicId);
    // 执行完取消订阅，避免一直重复订阅，这样会累加
    PubSub.unsubscribe('musicId');
  })

  // 发布消息给页面recomMusic
  PubSub.publish('switchType' , type)
 },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})