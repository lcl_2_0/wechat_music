// pages/recomMusic/recomMusic.js
import PubSub from 'pubsub-js'
import request from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    day:'', //日期
    month:'' , //
    recommendList:[],  //用户每日推荐数据
    index:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //检查用户是否登录，如果没有就跳转到用户登录界面
    let userInfo = wx.getStorageSync('userInfo');
    if(!userInfo){
      wx.showToast({
        title: '请先登录',
        icon:'none',
        success:()=>{
          //跳转登录
          wx.reLaunch({
            url: '/pages/login/login',
          })
        }
      })
    }

   //更新日期
   this.setData({
     day:new Date().getDay(),
     month:new Date().getMonth() + 1
   })
  //  console.log(this.data.day , this.data.month);

    //获取每日推荐数据
    this.getRecommendList();

    //订阅来自songDtai页面发布的消息
    PubSub.subscribe('switchType' , (msg , type) =>{
      let {recommendList , index} = this.data;

      // console.log( msg, type );
      if(type === 'pre'){
        // 上一首
        (index === 0) && (index = recommendList.length);
        index -=1;
      }else{
        // 下一首
        (index === recommendList.length - 1) && (index = -1);
        index +=1;
      }
      // 更新下标
      this.setData({
        index
      })
      let musicId = recommendList[index].id;
      // 将musicId回传给songDatil页面
       PubSub.publish('musicId' , musicId)

    });

  },
  //获取用户每日推荐数据
  async getRecommendList(){
    let getRecommendListData = await request('/recommend/songs');
    this.setData({
      recommendList:getRecommendListData.recommend
    })
  },

  //跳转到播放页面
  tosongDetatil(event){
  let {song , index} = event.currentTarget.dataset;
  this
  .setData({
    index
  })
  // console.log(song)
  //路由传参数：query
    wx.navigateTo({
      // 长度过长
      // url: '/pages/songDtai/songDtai?song='+JSON.stringify(song)
      url: '/pages/songDtai/songDtai?musicId='+song.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})