// 封装AJAX请求
// 默认发GET请求
import config from './config.js'
export default (url , data={} , method = 'GET') => {
  return new Promise((resolve , reject) =>{
  
    wx.request({
    //协议  域名  端口号
    url:config.host+ url,
    data,
    method,

    //设置请求头携带cookie
    header:{
      cookie:wx.getStorageSync('cookies')?wx.getStorageSync('cookies').find(item => item.indexOf('MUSIC_U') !== -1):''
    },
    success:(res)=>{
      // console.log('请求成功',res);
      if(data.isLogin){
        //将用户cookie存入本地
        wx.setStorage({
          key:'cookies',
          data:res.cookies
        })
      }
      resolve(res.data); //成功返回数据
    },
    fail:(err)=>{
      // console.log('请求失败:',err);
      reject(err) //失败状态
    }
  })
  })
  
}